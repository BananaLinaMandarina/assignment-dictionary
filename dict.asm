section .text
extern string_equals
extern string_length

global find_word

;args:
;rdi - string
;rsi - last word
find_word:
.loop:
    test rsi, rsi
    jz .not_found
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .found

    mov rsi, [rsi]
    jmp .loop
.found:
    mov rax, rsi
    ret
.not_found:
    xor rax, rax,
    ret

