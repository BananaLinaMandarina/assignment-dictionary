global string_length
global string_equals
global exit
global print_string
global read_word
global print_newline

section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .cycle:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .cycle
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rsi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    pop rdi
    syscall
    ret


print_newline:
    mov rdi, 0xA ; символ переноса строки
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    xor r9, r9
    push r9
    .cycle:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        inc r9
        cmp rax, 0
        je .print_number
        jmp .cycle
    .print_number:
        mov rdi, rsp
        call print_string
        add rsp, 8
        add rsp, r9   
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r8, rdi
    cmp rdi, 0
    jl .minus
    jmp print_uint
    .minus:
        mov rdi, '-'
        call print_char
        neg r8
        mov rdi, r8
        call print_uint
    .end:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .A:
        mov al, byte[rsi]
        cmp byte[rdi], al
        je .is_zero
        jmp .B
        .is_zero:
            cmp byte[rdi], 0
            je .C
            inc rdi
            inc rsi
            jmp .A
    .B:
        xor rax, rax
        ret
    .C:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, 0
    je .end
    mov al, byte[rsp]
    inc rsp
    ret
    .end:
        inc rsp
        ret
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    .cycle:
        push rdx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rdx
        cmp rax, 0
        je .yes
        cmp rdx, 0
        jne .next
        cmp rax, 0xA    ; код символа перевода строки
        je .cycle
        cmp rax, 0x9    ; код символа табуляции
        je .cycle
        cmp rax, 0x20   ; код символа пробела
        je .cycle
    .next:
        cmp rdx, rsi
        je .no
        cmp rax, 0xA
        je .yes
        cmp rax, 0x9
        je .yes
        cmp rax, 0x20
        je .yes
        mov r8, rax
        mov byte[rdx+rdi], r8b
        inc rdx
        jmp .cycle
    .yes:
        mov byte[rdx+rdi], 0
        mov rax, rdi
        ret
    .no:
        mov rax, 0
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    .cycle:
        xor r8, r8
        mov r8b, byte[rdi+rdx]
        cmp r8b, 0
        je .end
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end
        sub r8b, '0'
        push rdx
        mov r9, 10
        mul r9
        pop rdx
        add rax, r8    
        inc rdx
        jmp .cycle
    .end:
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .minus
    jmp parse_uint
    .minus:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .cycle:
        cmp byte[rdi+rax], 0
        je .ok
        cmp rax, rdx
        je .not_ok
        mov r8b, byte[rdi+rax]
        mov byte[rsi+rax], r8b
        inc rax
        jmp .cycle
    .ok:
        mov byte[rsi+rax], 0
        ret
    .not_ok:
        mov byte[rdi+rax], 0
        xor rax, rax
        ret





















