NASMFLAGS = -felf64 -o

all: lab2

lab2: lib.o dict.o main.o
	ld lib.o dict.o main.o -o lab2

lib.o: lib.inc
	nasm ${NASMFLAGS} lib.o lib.inc

dict.o: dict.asm
	nasm ${NASMFLAGS} dict.o dict.asm

main.o: colon.inc main.asm words.inc
	nasm ${NASMFLAGS} main.o main.asm

clean:
	rm -rf *.o lab2


