%define LAST_ELEM 0
%macro colon 2
    %ifid %2
        %2: dq LAST_ELEM
    %else
        %fatal "ERROR: identifier expected as second argument"
    %endif
    %ifstr %1
        %%elem_str: db %1, 0
    %else
        %fatal "ERROR: string expected as first argument"
    %endif
    %define LAST_ELEM %2
%endmacro

