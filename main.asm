section .data
%include 'colon.inc'
%include 'words.inc'

global _start

%include 'lib_extern.inc'

section .rodata
key_ask: db 'Enter key ', 0
found_key: db 'Found key: ', 0
not_found_key: db 'Key not found ', 10, 0
read_error_msg: db 'ERROR: cant read word', 10, 0

section .text

_start:
    mov rdi, key_ask
    mov rsi, 1 ;stdout
    call print_string
    sub rsp, 256
    mov rsi, 256
    mov rdi, rsp
    call read_word
    test rax, rax
    jz .read_error

    mov rdi, rax
    mov rsi, last_elem
    call find_word
    test rax, rax
    jz .not_found
    
    push rax
    mov rdi, found_key
    mov rsi, 1 ;stdout
    call print_string
    pop rax
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    mov rsi, 1 ;stdout
    call print_string
    call print_newline
    add rsp, 256
    call exit
.read_error:
    mov rdi, read_error_msg
    mov rsi, 2 ;stderr
    call print_string
    add rsp, 256
    call exit
.not_found:
    mov rdi, not_found_key
    mov rsi, 2 ;stderr
    call print_string
    add rsp, 256
    call exit

